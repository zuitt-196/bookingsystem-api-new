const express = require("express");
/*Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database*/
const mongoose = require("mongoose");
const app = express();
const port = 4000;

/*
	Mongoose Connection

	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. First, is the connection string to connect our api to our mongodb. Second, is an object used to add information between mongoose and mongodb.

	replace/change <password> in the connection string to your db user password

	just before the ? in the connection string, add the database name.
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.uctt7.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

//We will create notifications if the connection to the db is a success or failed.
let db = mongoose.connection;
//This is to show notification of an internal server error from MongoDB.
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
//IF the connection is open and successful, we will output a message in the terminal/gitbash:
db.once('open',()=>console.log("Connected to MongoDB."))

//express.json() to be able to handle the request body and parse it into JS Object
app.use(express.json());

//import our routes and use it as middleware.
//Which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
//use our routes and group them together under '/courses'
//our endpoints are now prefaced with /courses
app.use('/courses',courseRoutes);



// import ther user router
const userRoutes = require('./routes/userRoutes')
app.use('/users',userRoutes);




app.listen(port,()=>console.log("ExpressJS API running at localost:4000"))














// //create an server express API
// const express = require("express");

// // mogooes is an ODN library to let out ExprsssJS API manipulate a MongoDB database 
// const mongoose = require("mongoose")


// const app = express();
// const port= 4000;



// // Mongoees Connection 

//     // mongoose.connect() i a  method to
//  // replace/change in the connectiom string,add the database name 
//  mongoose.connect("mongodb+srv://admin:admin123@cluster0.uctt7.mongodb.net/bookingAPI?retryWrites=true&w=majority",
//  {
//         useNewUrlParser: true,
//         useUnifiedTopology:true
//  }); 
 
//  // we will create notification if the connection tothe db is a success or faild.
//  let db = mongoose.connection;
// // this is hoe to show notifiication of an inernal serve erro from mongoDb.
//  db.on('error',console.error.bind(console, "MongoDB Connection Error."));

// // if th connection is open and suceesfully, we wll output a message in terminal/gitbash

// db.once('open',()=> console.log("Connected to MongoDB"))






// //  express.json( to be able to hnadle the request body and parse it into JS )
// app.use(express.json());

// // import our routes 

// const courseRoutes = require('./routes/courseRoutes')
// //  console.log(courseRoutes);
// //use our ro
// // app.use('./routes/courseRoutes')
